from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ProgrammerBulkTest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('Quiz.urls')),
    # Match on the Quiz folder in a case insensitive way
    url(r'^(?i)quiz/', include('Quiz.urls')),
)
