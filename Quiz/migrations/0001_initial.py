# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'QuestionType'
        db.create_table(u'Quiz_questiontype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('has_choices', self.gf('django.db.models.fields.BooleanField')()),
            ('has_supporting_code', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'Quiz', ['QuestionType'])

        # Adding model 'Question'
        db.create_table(u'Quiz_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('instructions', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('question_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Quiz.QuestionType'])),
            ('is_demographic_question', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'Quiz', ['Question'])

        # Adding model 'Answer'
        db.create_table(u'Quiz_answer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Quiz.Question'])),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('answer', self.gf('django.db.models.fields.TextField')()),
            ('session', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'Quiz', ['Answer'])

        # Adding model 'QuestionChoice'
        db.create_table(u'Quiz_questionchoice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Quiz.Question'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'Quiz', ['QuestionChoice'])


    def backwards(self, orm):
        # Deleting model 'QuestionType'
        db.delete_table(u'Quiz_questiontype')

        # Deleting model 'Question'
        db.delete_table(u'Quiz_question')

        # Deleting model 'Answer'
        db.delete_table(u'Quiz_answer')

        # Deleting model 'QuestionChoice'
        db.delete_table(u'Quiz_questionchoice')


    models = {
        u'Quiz.answer': {
            'Meta': {'object_name': 'Answer'},
            'answer': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Quiz.Question']"}),
            'session': ('django.db.models.fields.TextField', [], {})
        },
        u'Quiz.question': {
            'Meta': {'object_name': 'Question'},
            'code': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instructions': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'is_demographic_question': ('django.db.models.fields.BooleanField', [], {}),
            'question_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Quiz.QuestionType']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'Quiz.questionchoice': {
            'Meta': {'object_name': 'QuestionChoice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Quiz.Question']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Quiz.questiontype': {
            'Meta': {'object_name': 'QuestionType'},
            'has_choices': ('django.db.models.fields.BooleanField', [], {}),
            'has_supporting_code': ('django.db.models.fields.BooleanField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['Quiz']