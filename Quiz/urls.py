from django.conf.urls import patterns, url
from Quiz import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<question_id>\d+)/$', views.question, name='quiz'),
    url(r'^(?P<question_id>\d+)/answer/$', views.answer, name='answer')
)