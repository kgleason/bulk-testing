from django.contrib import admin
from Quiz.models import QuestionType, Question, QuestionChoice
# Register your models here.

admin.site.register(QuestionType)
admin.site.register(Question)
admin.site.register(QuestionChoice)