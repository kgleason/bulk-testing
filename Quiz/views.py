from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext, loader
import uuid

from Quiz.models import Question, QuestionChoice, QuestionType, Answer

# Create your views here.
def index(request):
    question_list = Question.objects.all()
    
    quid = request.session.get('QuizUUID', None)
    if quid == None:
        # Set the session key to a random UUID
        request.session['QuizUUID'] = str(uuid.uuid4())
    
    return render(request, 'questions/index.html', {'question_list':question_list})
    
def question(request, question_id, error_message=None):
    try:
        q = Question.objects.get(pk=question_id)
        t = QuestionType.objects.get(id=q.question_type_id)
        p, created = Answer.objects.get_or_create(question=q,session=request.session['QuizUUID'], defaults={'answer':"", 'language':""})
        sp = None if created else created
        context = {'question':q, 'question_type':t, 'previous_answer':p, 'show_previous':sp, 'error_message':error_message}
    except Question.DoesNotExist:
        question_list = Question.objects.all()
        return render(request, 'questions/index.html', {'question_list':question_list})
    return render(request, 'questions/question.html', context)
    
def answer(request, question_id, next_question=None):
    #return HttpResponse("This is the answer page") 
    try:
        r = request.POST['answer']
    except (KeyError):
        #question(request, question_id, error_message="You did not submit an answer.")
        q = Question.objects.get(pk=question_id)
        t = QuestionType.objects.get(id=q.question_type_id)
        p, created = Answer.objects.get_or_create(question=q,session=request.session['QuizUUID'], defaults={'answer':"", 'language':""})
        error_message = "You did not submit an answer"
        context = {'question':q, 'question_type':t, 'previous_answer':p, 'error_message':error_message}
        return render(request, 'questions/question.html', context)
    else:
        q = get_object_or_404(Question, id=question_id)
        if q.question_type.name == "LongAnswer":
            lang = request.POST['lang']
        else:
            lang = ""
        a = get_object_or_404(Answer, question=q, session=request.session['QuizUUID'])
        a.question = q
        a.session = request.session['QuizUUID']
        a.answer = str(r)
        a.language = lang
        a.save()
    
    return render(request, 'questions/answer.html', {'answer':a, 'previous':q.id-1, 'next':q.id+1})

        
    