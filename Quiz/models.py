from django.db import models

# Create your models here.
class QuestionType(models.Model):
    name = models.CharField(max_length=50)
    has_choices = models.BooleanField()
    has_supporting_code = models.BooleanField()
    
    def __unicode__(self):
        return self.name

class Question(models.Model):
    instructions = models.TextField(blank=True,null=True)
    text = models.TextField(blank=True,null=True)
    code = models.TextField(blank=True,null=True)
    question_type = models.ForeignKey(QuestionType)
    is_demographic_question = models.BooleanField()
    
    def __unicode__(self):
        return self.text
    
class Answer(models.Model):
    question = models.ForeignKey(Question)
    language = models.CharField(max_length=50)
    answer = models.TextField()
    session = models.TextField()
    
    def __unicode__(self):
        return self.answer
    
class QuestionChoice(models.Model):
    question = models.ForeignKey(Question)
    text = models.TextField()
    
    def __unicdoe__(self):
        return self.question